import React, {Component} from "react";
import { SafeAreaView, StyleSheet, TextInput, Button, Text,TouchableOpacity } from "react-native";


class App extends Component {
  constructor (props){
    super (props);
    
    this.state = {
      firstname:"",
      lastname:"",
      fulltext:null
    };
  }

  
  firstnameTextChange = (inputText) =>{
    this.setState({firstname : inputText})
  }

  lastnameTextChange = (inputText) =>{
    this.setState({lastname : inputText})
  }
  updateFullText = () => {
    let text1 = this.state.firstname;
    let text2 = this.state.lastname;
    if (text1 !== null && text2 !== null && text1 !== ' ' && text2 !== ' ') {
      this.setState({fulltext: text1 + ' ' + text2});
    } else {
      Alert.alert('Info', 'Input tidak boleh kosong');
    }
  };
 
  render() {
    return (
      <SafeAreaView
      style={styles.halaman}>
        <Text style={styles.text}>
        Helva Mardiana</Text>
        <TextInput
          style={styles.input}
          onChangeText={this.firstnameTextChange}
          placeholder = "Your First Name"
          placeholderTextColor={'grey'}
          
        />
        <TextInput
          style={styles.input}
          onChangeText={this.lastnameTextChange}
          placeholder = "Your Last Name"
          placeholderTextColor={'grey'}
          
        />
        <Text> My Name Is : {this.state.fulltext}</Text>
        <TouchableOpacity
          onPress={() => this.updateFullText()}
          style={styles.btn}>
          <Text>Save</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    backgroundColor: "white",
    height: 50,
    margin: 12,
    borderWidth: 2,
    padding: 10,
    borderRadius:5,
    color: "black"
  },
  halaman :{
    height:900,
    backgroundColor:"grey",
    paddingVertical: 150,
  },
  btn: {
    backgroundColor: 'black',
    padding: 10,
    marginLeft:100,
    marginRight:100,
    justifyContent:"center",
    borderRadius:5,
  },
  text: {
    fontSize: 40,
    paddingBottom:60,
    marginLeft: 40,
    marginTop:20,
    color: "black",
  },

});

export default App;